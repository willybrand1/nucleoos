<?php
include_once './defines.php';

class DALQueryResult {
 
    private $_results = array();

    public function __construct(){}
   
    public function __set($var,$val){
        $this->_results[$var] = $val;
    }
   
    public function __get($var){  
        if (isset($this->_results[$var])){
            return $this->_results[$var];
        }
        else{
            return null;
        }
    }
}

class Database extends PDO{
    protected static $db;

    public function __construct($banco){
        $ini = parse_ini_file(DIR_INI.'config.ini',true);

        if ($ini === false) {
            die("Erro ao ler arquivo de configuração do banco de dados");
        }

        $host = $ini[$banco]['host'];
        $port = $ini[$banco]['port'];
        $dbname = $ini[$banco]['dbname'];
        $user = $ini[$banco]['user'];
        $password = $ini[$banco]['password'];
        $drive = $ini[$banco]['drive'];
        
        if($drive == 'mysql'){
            try{
                $this->db = new PDO("$drive:host=$host:$port;dbname=$dbname", "$user", "$password");
            }catch(PDOException $e){
                die("Erro de conexão: " . $e->getMessage());
            }
        }else if($drive == 'psql'){
            try{
                $this->db = pg_connect("host=$host port=$port dbname=$dbname user=$user password=$password");
            }catch(Exception $e){
                die("Erro de conexão: " . $e->getMessage());
            }
        }
    }

    public function query($sql){
        if(empty($sql)){
            return false;
        }

        $results = array();
        $textosql = trim($sql);
        $textosql = strtolower($textosql);

        $rs = $this->db->query($textosql);
        //select
        if(substr($sql,0,6) == "select"){
            while ($row = $rs->fetchAll(PDO::FETCH_ASSOC)){

                $result = new DALQueryResult();
             
                foreach ($row as $k=>$v){
                    $result->$k = $v;
                }
             
                $results[] = $result;
            }
            return $results; 
        }else{
            return true;
        }
    }

    public function query_pg($sql){
        if(empty($sql)){
            return false;
        }

        $textosql = trim($sql);
        $textosql = strtolower($textosql);

        $rs = pg_query($this->db,$textosql);
        
        if(pg_num_rows($rs) < 1){
            return false;
        }else{
            $row = pg_fetch_all($rs);
            return $row;
        }
    }

    public function close(){
        $db = $this->db;
        
        $c = mysqli_close($db);
        
        return $c;
    }

    public function close_pg(){
        $db = $this->db;
        
        $c = pg_close($db);
    
        return $c;
    }
}
?>