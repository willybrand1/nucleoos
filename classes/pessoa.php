<?php
include_once './defines.php';
include_once DIR_FUNCOES.'funcoes.php';

class Pessoa{
    protected $nome;
    protected $cpf;
    protected $dta_nascimento;
    protected $rg;
    protected $sexo;
    protected $email;
    protected $empresa;
    protected $cnpj;
    protected $telefone;

    

    /**
     * Get the value of nome
     */ 
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Set the value of nome
     *
     * @return  self
     */ 
    public function setNome($nome)
    {
        if(empty($nome)){
            return "1";
        }else{
            $this->nome = $nome;
            return $this;
        }
    }

    /**
     * Get the value of cpf
     */ 
    public function getCpf()
    {
        return $this->cpf;
    }

    /**
     * Set the value of cpf
     *
     * @return  self
     */ 
    public function setCpf($cpf)
    {
        if(empty($cpf)){
            return "1";
        }else if(!validaCpf($cpf)){
            return "2";
        }else{
            $this->cpf = $cpf;
            return $this;
        }
    }

    /**
     * Get the value of dta_nascimento
     */ 
    public function getDta_nascimento()
    {
        return $this->dta_nascimento;
    }

    /**
     * Set the value of dta_nascimento
     *
     * @return  self
     */ 
    public function setDta_nascimento($dta_nascimento)
    {
        $this->dta_nascimento = $dta_nascimento;

        return $this;
    }

    /**
     * Get the value of rg
     */ 
    public function getRg()
    {
        return $this->rg;
    }

    /**
     * Set the value of rg
     *
     * @return  self
     */ 
    public function setRg($rg)
    {
        $this->rg = $rg;

        return $this;
    }

    /**
     * Get the value of sexo
     */ 
    public function getSexo()
    {
        return $this->sexo;
    }

    /**
     * Set the value of sexo
     *
     * @return  self
     */ 
    public function setSexo($sexo)
    {
        $this->sexo = $sexo;

        return $this;
    }

    /**
     * Get the value of email
     */ 
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set the value of email
     *
     * @return  self
     */ 
    public function setEmail($email)
    {
        if(empty($email)){
            return "1";
        }else if(!validaEmail($email)){
            return "2";
        }else{
            $this->email = $email;
            return $this;
        }
    }

    /**
     * Get the value of empresa
     */ 
    public function getEmpresa()
    {
        return $this->empresa;
    }

    /**
     * Set the value of empresa
     *
     * @return  self
     */ 
    public function setEmpresa($empresa)
    {
        if(empty($empresa)){
            return "1";
        }else{
            $this->empresa = $empresa;
            return $this;
        }
    }

    /**
     * Get the value of cnpj
     */ 
    public function getCnpj()
    {
        return $this->cnpj;
    }

    /**
     * Set the value of cnpj
     *
     * @return  self
     */ 
    public function setCnpj($cnpj)
    {
        if(empty($cnpj)){
            return "1";
        }else if(!validaCNPJ($cnpj)){
            return "2";
        }else{
            $this->cnpj = $cnpj;
            return $this;
        }
    }

    /**
     * Get the value of telefone
     */ 
    public function getTelefone()
    {
        return $this->telefone;
    }

    /**
     * Set the value of telefone
     *
     * @return  self
     */ 
    public function setTelefone($telefone)
    {
        if(empty($telefone)){
            return "1";
        }else{
            $this->telefone = $telefone;
            return $this;
        }
    }
}
