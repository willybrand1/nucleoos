<?php
define('BANCO','estudos');

$dir = realpath(dirname(__FILE__)."/")."/";
$dir_root = realpath(dirname(__FILE__)."/..")."/";

define("DIR_ROOT",$dir_root);
define("DIR_BASE",$dir);
define('DIR_CLASSES',$dir."classes/");
define('DIR_SERVICOS',$dir."servicos/");
define('DIR_CSS',$dir."css/");
define('DIR_FUNCOES',$dir."funcoes/");
define('DIR_JS',$dir."js/");
define('DIR_VISAO',$dir."visao/");
define('DIR_INI',$dir."ini/");
?>