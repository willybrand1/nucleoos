<?php
include_once '../defines.php';
include_once DIR_VISAO.'header.php';
?>
<script type="text/javascript">

function salvaCadastroPessoa(){
    var error = 0;
    var arrErro = new Array();
    var arrOk = new Array();

    var nome = $("#txtNome").val();
    var cpf = $("#txtCpf").val();
    var dta_nascimento = $("#txtDta_nascimento").val();
    var rg = $("#txtRg").val();
    var sexo = $("#txtSexo").val();
    var email = $("#txtEmail").val();
    var empresa = $("#txtEmpresa").val();
    var cnpj = $("#txtCnpj").val();
    var telefone = $("#txtTelefone").val();

    arrErro = [];

    if(nome == ""){
        error += 1;
        arrErro.push("1spNOME");
    }else{
        arrOk.push("spNOME");
    }

    if(cpf == ""){
        error += 1;
        arrErro.push("1spCPF");
    }else if(!validaCpf(cpf)){
        error += 1;
        arrErro.push("2spCPF");
    }else{
        arrOk.push("spCPF");
    } 

    if(email == ""){
        error += 1;
        arrErro.push("1spE-MAIL");
    }else{
        arrOk.push("spE-MAIL");
    }

    if(empresa == ""){
        error += 1;
        arrErro.push("1spEMPRESA");
    }else{
        arrOk.push("1spEMPRESA");
    }

    if(cnpj == ""){
        error += 1;
        arrErro.push("1spCNPJ");
    }else if(!validarCNPJ(cnpj)){
        error += 1;
        arrErro.push("2spCNPJ");
    }else{
        arrOk.push("spCNPJ");
    }

    if(telefone == ""){
        error += 1;
        arrErro.push("1spTELEFONE");
    }else{
        arrOk.push("spTELEFONE");
    }

    if(error > 0){
        $(arrErro).each(function(index,el){
            if(el.substring(0,1) == 1){
                $("#"+el.substring(1)).html('<span id="'+el.substring(1)+'" class="erroMsg">O campo '+(el.substring(3).replace("_"," "))+' deve ser preenchido.</span>');
            }

            if(el.substring(0,1) == 2){
                $("#"+el.substring(1)).html('<span id="'+el.substring(1)+'" class="erroMsg">O campo '+(el.substring(3).replace("_"," "))+' está inválido.</span>');
            }
        });

        $(arrOk).each(function(index,el){
            $("#"+el).html('<span id="'+el+'"></span>');
        });
    }else{
        $.ajax({
            url: '../estudos/servicos/ctrl-cadastro-pessoa.php',
            dataType: 'json',
            type: 'POST',
            data: {
                nome:nome,
                cpf:cpf,
                dta_nascimento:dta_nascimento,
                rg:rg,
                sexo:sexo,
                email:email,
                empresa:empresa,
                cnpj:cnpj,
                telefone:telefone,
            },
            success: function(dados){
                var d = dados.dados;
                var e = dados.erros;
                var a = dados.acertos;
                var cod = d.cod;
                var msg = d.msg;
                var txt = '';
                txt += '<div class="container">';
                    txt += '<div class="row">';
                        txt += '<div class="col-md-12 col-sm-12">';
                            txt += '<h3>'+msg+'</h3>';
                        txt += '</div>';
                    txt += '</div>';
                txt += '</div>';

                if(cod == 3){
                    for(var z=0; z<a.length; z++){
                        $("#"+a[z]).html('<span id="'+a[z]+'"></span>');
                    }

                    abreJanelaModalTexto('Sucesso', txt,'',1,'',setTimeout(function(){
                        reloadPage();
                    },1500));
                }else if(cod == 2){
                    abreJanelaModalTexto('Erro ao inserir', txt,'',1,'',setTimeout(function(){
                        reloadPage();
                    },1500));
                }else if(cod == 1){
                    for(var i=0; i<e.length; i++){
                        if(e[i].substring(0,1) == 1){
                            $("#"+e[i].substring(1)).html('<span id="'+e[i].substring(1)+'" class="erroMsg">O campo '+(e[i].substring(3).replace("_"," "))+' deve ser preenchido.</span>');
                        }

                        if(e[i].substring(0,1) == 2){
                            $("#"+e[i].substring(1)).html('<span id="'+e[i].substring(1)+'" class="erroMsg">O campo '+(e[i].substring(3).replace("_"," "))+' está inválido.</span>');
                        }
                    }

                    for(var x=0; x<a.length; x++){
                        $("#"+a[x]).html('<span id="'+a[x]+'"></span>');
                    }
                }
            }
        });
    }
}

function validaCpf(strCPF) {
    strCPF = strCPF.replace(/[^\d]+/g,'');
    var Soma;
    var Resto;
    Soma = 0;
    if (strCPF == "00000000000" || strCPF == "11111111111" || strCPF == "22222222222" || strCPF == "33333333333" || strCPF == "44444444444" || strCPF == "55555555555" || strCPF == "66666666666" || strCPF == "77777777777" || strCPF == "88888888888" || strCPF == "99999999999") 
        return false;
        
    for (i=1; i<=9; i++) Soma = Soma + parseInt(strCPF.substring(i-1, i)) * (11 - i);
        Resto = (Soma * 10) % 11;
    
        if ((Resto == 10) || (Resto == 11))  Resto = 0;
        if (Resto != parseInt(strCPF.substring(9, 10)) ) return false;
    
        Soma = 0;
        for (i = 1; i <= 10; i++) Soma = Soma + parseInt(strCPF.substring(i-1, i)) * (12 - i);
        Resto = (Soma * 10) % 11;
    
        if ((Resto == 10) || (Resto == 11))  Resto = 0;
        if (Resto != parseInt(strCPF.substring(10, 11) ) ) return false;
        return true;
}

function validarCNPJ(cnpj) {
    cnpj = cnpj.replace(/[^\d]+/g,'');

    if(cnpj == '') return false;
    
    if (cnpj.length != 14)
        return false;

    // Elimina CNPJs invalidos conhecidos
    if (cnpj == "00000000000000" || 
        cnpj == "11111111111111" || 
        cnpj == "22222222222222" || 
        cnpj == "33333333333333" || 
        cnpj == "44444444444444" || 
        cnpj == "55555555555555" || 
        cnpj == "66666666666666" || 
        cnpj == "77777777777777" || 
        cnpj == "88888888888888" || 
        cnpj == "99999999999999")
        return false;
        
    // Valida DVs
    tamanho = cnpj.length - 2
    numeros = cnpj.substring(0,tamanho);
    digitos = cnpj.substring(tamanho);
    soma = 0;
    pos = tamanho - 7;
    for (i = tamanho; i >= 1; i--) {
    soma += numeros.charAt(tamanho - i) * pos--;
    if (pos < 2)
            pos = 9;
    }
    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
    if (resultado != digitos.charAt(0))
        return false;
        
    tamanho = tamanho + 1;
    numeros = cnpj.substring(0,tamanho);
    soma = 0;
    pos = tamanho - 7;
    for (i = tamanho; i >= 1; i--) {
    soma += numeros.charAt(tamanho - i) * pos--;
    if (pos < 2)
            pos = 9;
    }
    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
    if (resultado != digitos.charAt(1))
        return false;
            
    return true;
 
}

function abreJanelaModalTexto(titulo, conteudo,id_campo_foco,modal_large,callback_onclose,callback,modal_footer){
    var idv = Math.floor((Math.random() * 100) + 1);
    var str_modal_large='';
    if(typeof modal_large!='undefined'){
        if(parseInt(modal_large)==1){
            str_modal_large=' modal-lg';
        }
    }
    var txt = '<div class="modal fade" id="myModal' + idv + '" tabindex="-1" role="dialog" aria-labelledby="myModal' + idv + 'Label" aria-hidden="true">' +
        '  <div class="modal-dialog'+str_modal_large+'">' +
        ' <div class="modal-content">' +
        ' <div class="modal-header">' +
        ' 	<h4 class="modal-title">' + titulo + '</h4>' +
        ' </div>' +
        ' <div class="modal-body"><span id="loc-proc-jan">&nbsp;</span>' +
        '	<div id="dv1" role="alert">' + conteudo + '</div>' +
        ' </div>' +
            ' <div class="modal-footer">' +(typeof modal_footer!='undefined'?modal_footer:'')+
    ' 				</div>' +
    ' 	</div>' +
    '  </div>' +
    ' </div>';

    $('#local-janela').html(txt);
    $('#myModal' + idv).modal('show');
    if(id_campo_foco!=''){
        setTimeout(function(){
            $('#'+id_campo_foco).focus();
        },500);
    }

    if(typeof callback_onclose==='function'){
        $('#myModal' + idv).on('hidden.bs.modal', function(){
            callback_onclose();
        });
    }

    if(typeof callback==='function'){            
            callback();            
    }
}

function reloadPage(){
    location.href = 'index.php';
}

function showVertical(cod,l){
    var txt_var = '';
    var txt_vert = '';
    
    if(cod == 1){
        if(l == "pt-br"){
            txt_vert += 'Hotel';
        }else{
            txt_vert += 'Hotel';
        }
        
        txt_var += '<div class="col-md-12 col-sm-12">';
            txt_var += '<center><img src="images/portfolio-img1.jpg" alt="portfolio img" style="width:100%;"></center>';
            txt_var += '<br><br>';
        txt_var += '</div>';
        txt_var += '<div class="col-md-12 col-sm-12">';
            txt_var += '<center><p style="text-align:justify;">';
                if(l == "pt-br"){
                    txt_var += 'A solução de gerenciamento de hotéis permite gerenciar facilmente a disponibilidade de quartos,'+
                    'o estoque e os dados pessoais. Além disso, se o império de negócios se estender por várias regiões '+
                    'geográficas e hotéis, você poderá obter uma visualização única centralizada de ocupação, '+
                    'receita por quarto disponível e outras métricas de oferta e demanda que determinam o desempenho '+
                    'de seu hotel com a solução completa da NucleoOS.';
                }else{
                    txt_var += 'The hotel management solution allows you to easily manage room availability,'+
                    'inventory and personal data. In addition, if the business empire extends across multiple regions '+
                    'geographic areas and hotels, you can get a centralized single view of occupancy, '+
                    'revenue per available room and other supply and demand metrics that determine the performance '+
                    'of your hotel with the complete NucleoOS solution.';
                }
            txt_var += '</p></center>';
        txt_var += '</div>';
        txt_var += '<div>';
            txt_var += '<br><br><center><input type="button" class="btn btn-warning" value="Fechar" data-dismiss="modal"></center>';
        txt_var += '</div>';
    }else if(cod == 2){
        if(l == "pt-br"){
            txt_vert += 'Governo';
        }else{
            txt_vert += 'Government';
        }
        
        txt_var += '<div class="col-md-12 col-sm-12">';
            txt_var += '<center><img src="images/portfolio-img2.jpg" alt="portfolio img" style="width:100%;"></center>';
            txt_var += '<br><br>';
        txt_var += '</div>';
        txt_var += '<div class="col-md-12 col-sm-12">';
            txt_var += '<center><p style="text-align:justify;">';
                if(l == "pt-br"){
                    txt_var += 'Orgãos públicos, agências reguladoras, prefeituras, governo estadual, federal, gestão orçamentária, relatórios customizados, Gestão Financeira, fluxo de caixa, gestão de servidores, departamento pessoal, controle de presença, folha de pagamento, e-social, controle de despesas, gestão de contratos e licitações, controle de correspondências e malotes, requisições de materiais, gestão de projetos, Gestão de Obras, Gestão Hospitalar, Gestão de Frotas, Georeferenciamento. Arquitetura de reduçao de custo de desenvolvimento e aceleraçao de novos produtos a desenvolvimento.';
                }else{
                    txt_var += 'Public departments, regulatory agencies, prefectures, state government, federal, budget management, customized reports, financial management, cash flow, server management, personal department, presence control, payroll, social, expense control, management of contracts and bids, control of mails and bags, material requisitions, project management, Works Management, Hospital Management, Fleet Management, Georeferencing. Architecture of cost reduction of development and acceleration of new products to development.';
                }
            txt_var += '</p></center>';
        txt_var += '</div>';
        txt_var += '<div>';
            txt_var += '<br><br><center><input type="button" class="btn btn-warning" value="Fechar" data-dismiss="modal"></center>';
        txt_var += '</div>';
    }else if(cod == 3){
        if(l == "pt-br"){
            txt_vert += 'Saúde';
        }else{
            txt_vert += 'Health';
        }
        
        txt_var += '<div class="col-md-12 col-sm-12">';
            txt_var += '<center><img src="images/portfolio-img3.jpg" alt="portfolio img" style="width:100%;"></center>';
            txt_var += '<br><br>';
        txt_var += '</div>';
        txt_var += '<div class="col-md-12 col-sm-12">';
            txt_var += '<center>';
                if(l == "pt-br"){
                    txt_var += '<p style="text-align:justify;">A solução  Saude foi criada para atender aos requisitos exclusivos do setor de assistência médica. A precisão e a disponibilidade de informações são essenciais para o sucesso desse domínio. Nossa solução garante isso com os 3 módulos principais:</p>';
                    txt_var += '<ul>';
                    txt_var += '<li>Registro Médico Eletrônico (EMR)</li>';
                    txt_var += '<li>Sistema de Informação Hospitalar (HIS)</li>';
                    txt_var += '<li>Sistema de Informação em Saúde</li>';
                    txt_var += '</ul>';
                    txt_var += '<p style="text-align:justify;">O módulo centralizado permitirá acesso mais rápido e preciso aos registros dos pacientes, o que ajudará os médicos a se adaptarem rapidamente em caso de situação de emergência. Os sistemas automatizados também ajudam o hospital / clínica a ser mais produtivo a custos mais baixos.</p>';
                }else{
                    txt_var += '<p style="text-align:justify;">The Saude solution was created to meet the unique requirements of the healthcare industry. The accuracy and availability of information is essential to the success of this domain. Our solution guarantees this with the 3 main modules:</p>';
                    txt_var += '<ul>';
                    txt_var += '<li>Electronic Medical Record (EMR)</li>';
                    txt_var += '<li>Hospital Information System (HIS)</li>';
                    txt_var += '<li>Health Information System</li>';
                    txt_var += '</ul>';
                    txt_var += '<p style="text-align:justify;">The centralized module will allow faster, more accurate access to patient records, which will help doctors quickly adapt to an emergency situation. Automated systems also help the hospital / clinic to be more productive at lower costs.</p>';
                }
            txt_var += '</center>';
        txt_var += '</div>';
        txt_var += '<div>';
            txt_var += '<br><br><center><input type="button" class="btn btn-warning" value="Fechar" data-dismiss="modal"></center>';
        txt_var += '</div>';
    }else if(cod == 4){
        if(l == "pt-br"){
            txt_vert += 'Controle de frotas';
        }else{
            txt_vert += 'Fleet control';
        }
        
        txt_var += '<div class="col-md-12 col-sm-12">';
            txt_var += '<center><img src="images/portfolio-img4.jpg" alt="portfolio img" style="width:100%;"></center>';
            txt_var += '<br><br>';
        txt_var += '</div>';
        txt_var += '<div class="col-md-12 col-sm-12">';
            txt_var += '<center><p style="text-align:justify;">';
                if(l == "pt-br"){
                    txt_var += 'Ganhar a vantagem no domínio competitivo da frota e do transporte requer uma visão aguçada de todos os aspectos operacionais. A solução de gerenciamento de frota end-to-end da Odoo ajuda você na rápida tomada de decisões quando se trata de seu pessoal, frota ou tarefas administrativas. O resultado - operações de frota de bom funcionamento que encantam os clientes e o pessoal interno. <br>A solução é voltada para um nível profundo de personalização para que ela resolva seus desafios de negócios precisos e ajude você a expandir seus negócios com a assistência da tecnologia.';
                }else{
                    txt_var += 'Gaining advantage in the competitive field of fleet and transportation requires a keen insight into all operational aspects. Odoo\'s end-to-end fleet management solution helps you make quick decisions when it comes to your personnel, fleet, or administrative tasks. The result - well-functioning fleet operations that delight customers and internal staff. <br> The solution is geared towards a deep level of customization so that it addresses your precise business challenges and helps you grow your business with the assistance of technology.';
                }
            txt_var += '</p></center>';
        txt_var += '</div>';
        txt_var += '<div>';
            txt_var += '<br><br><center><input type="button" class="btn btn-warning" value="Fechar" data-dismiss="modal"></center>';
        txt_var += '</div>';
    }
    abreJanelaModalTexto(txt_vert, txt_var,'',1,'','');
}
</script>