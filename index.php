<?php
include_once './defines.php';
include_once DIR_BASE.'Banco.php';

$l = isset($_REQUEST['l']) ? $_REQUEST['l'] : 'pt-br';
?>
<html>
<head>
	<meta charset="utf-8">
	<title>Nucleo OS</title>
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="keywords" content="">
	<meta name="description" content="">
	<link rel="stylesheet" type="text/css" href="./fontawesome/css/all.css">
<!--

Template 2075 Digital Team

http://www.tooplate.com/view/2075-digital-team

-->
	<link rel="stylesheet" href="./css/bootstrap.min.css">
	<link rel="stylesheet" href="./css/font-awesome.min.css">
	<link rel="stylesheet" href="./css/animate.min.css">
	<link rel="stylesheet" href="./css/et-line-font.css">
	<link rel="stylesheet" href="./css/nivo-lightbox.css">
	<link rel="stylesheet" href="./css/nivo_themes/default/default.css">
	<link rel="stylesheet" href="./css/style.css">
	<link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500' rel='stylesheet' type='text/css'>
</head>
<body data-spy="scroll" data-target=".navbar-collapse" data-offset="50">

<!-- preloader section -->
<div class="preloader">
	<div class="sk-spinner sk-spinner-circle">
       <div class="sk-circle1 sk-circle"></div>
       <div class="sk-circle2 sk-circle"></div>
       <div class="sk-circle3 sk-circle"></div>
       <div class="sk-circle4 sk-circle"></div>
       <div class="sk-circle5 sk-circle"></div>
       <div class="sk-circle6 sk-circle"></div>
       <div class="sk-circle7 sk-circle"></div>
       <div class="sk-circle8 sk-circle"></div>
       <div class="sk-circle9 sk-circle"></div>
       <div class="sk-circle10 sk-circle"></div>
       <div class="sk-circle11 sk-circle"></div>
       <div class="sk-circle12 sk-circle"></div>
    </div>
</div>

<!-- navigation section -->
<section class="navbar navbar-fixed-top custom-navbar" role="navigation">
	<div class="container">
		<div class="navbar-header">
			<button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="icon icon-bar"></span>
				<span class="icon icon-bar"></span>
				<span class="icon icon-bar"></span>
			</button>
			<a href="#" class="navbar-brand" style="text-shadow: 2px 1px 11px #000000;">Nucleo <b style="color:#ffff00;text-shadow: 2px 1px 11px #000000;">O</b><b style="color:#00cc00;text-shadow: 2px 1px 11px #000000;">S</b></a>
		</div>
		<div class="collapse navbar-collapse">
			<ul class="nav navbar-nav navbar-right">
				<li><a href="#home" style="text-shadow: 2px 1px 11px #000000;" class="smoothScroll" aria-controls="home" role="tab" data-toggle="tab">HOME</a></li>
				<li><a href="#portfolio" style="text-shadow: 2px 1px 11px #000000;" class="smoothScroll" aria-controls="home" role="tab" data-toggle="tab"><?=(($l !== 'pt-br') ? 'FEATURES' : 'FUNCIONALIDADES')?></a></li>
				<li><a href="#about" style="text-shadow: 2px 1px 11px #000000;" class="smoothScroll" aria-controls="home" role="tab" data-toggle="tab"><?=(($l !== 'pt-br') ? 'WHO WE ARE' : 'QUEM SOMOS')?></a></li>
				<li><a href="#verticais" style="text-shadow: 2px 1px 11px #000000;" class="smoothScroll" aria-controls="home" role="tab" data-toggle="tab"><?=(($l !== 'pt-br') ? 'VERTICAL' : 'VERTICAIS')?></a></li>
				<li><a href="#pricing" style="text-shadow: 2px 1px 11px #000000;" class="smoothScroll" aria-controls="home" role="tab" data-toggle="tab"><?=(($l !== 'pt-br') ? 'PRODUCTS' : 'PRODUTOS')?></a></li>
				<li><a href="#contact" style="text-shadow: 2px 1px 11px #000000;" class="smoothScroll" aria-controls="home" role="tab" data-toggle="tab"><?=(($l !== 'pt-br') ? 'PARTNERSHIP' : 'PARCERIA')?></a></li>
				<li><a href="./index.php?l=pt-br" style="text-shadow: 2px 1px 11px #000000;" class="smoothScroll"><img src="../nucleoos/images/brasil.png" style="width:30px;" title="Idioma português brasileiro."></a></li>
				<li><a href="./index.php?l=en" style="text-shadow: 2px 1px 11px #000000;" class="smoothScroll"><img src="../nucleoos/images/usa.jpg" style="width:30px;" title="Language english international."></a></li>
				<li><a href="#final" style="text-shadow: 2px 1px 11px #000000;" class="smoothScroll" aria-controls="home" role="tab" data-toggle="tab"></a></li>
			</ul>
		</div>
	</div>
</section>

<!-- home section -->
<section id="home">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<?php
				if($l == 'pt-br'){
				?>
				<h1 style="font-family: Segoe UI Light 2.25em;text-shadow: 2px 1px 11px #000000;">Nucleo Fusion Middleware</h1>
				<h2 style="font-family: Segoe UI Light 1.05em;text-shadow: 2px 1px 11px #000000;">Nova geração para código aberto de desenvolvimento de sistema</h2>
				<hr>
				<a href="#pricing" class="smoothScroll btn btn-danger">BAIXAR</a>
				<?php
				}else if($l == 'en'){
				?>
				<h1 style="font-family: Segoe UI Light 2.25em;text-shadow: 2px 1px 11px #000000;">Nucleo Fusion Middleware</h1>
				<h2 style="font-family: Segoe UI Light 1.05em;text-shadow: 2px 1px 11px #000000;">New open source generation for system developing</h2>
				<hr>
				<a href="#pricing" class="smoothScroll btn btn-danger">DOWNLOAD</a>
				<?php
				}
				?>
			</div>
		</div>
	</div>		
</section>

<!-- work section -->
<section id="portfolio">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<div class="section-title">
					<h1 class="heading bold"><?=(($l == 'pt-br') ? 'NUCLEO FUSION MIDDLEWARE' : 'NUCLEO FUSION MIDDLEWARE')?></h1>
					<hr>
				</div>
			</div>
		</div>
		<div class="jumbotron jumbotron-fluid">
			<div class="container">
				<?php
				if($l == "pt-br"){
				?>
				<div class="row">
					<div class="col-md-12 col-sm-12">
						<img src="./images/SCHEMA_BUSINESS-0.png" alt="esquema nucleoos">
					</div>
				</div>
				<?php
				}else{
				?>
				<div class="row">
					<div class="col-md-12 col-sm-12">
						<img src="./images/SCHEMA_BUSINESS-1.png" alt="scheme nucleoos">
					</div>
				</div>
				<?php
				}
				?>
				<br><br>
				<?php
				if($l == "pt-br"){
				?>
				<div class="row">
					<div class="col-md-12 col-sm-12">
						<p style="text-align:justify;margin: 0px 100px 0px 100px;">Os principais caracteristicas do Nucleo Fusion Middleware são incorporados na base core de todos os módulos , sendo tudo em um só código, sem necessidade de integração.</p>
						<p style="text-align:justify;margin: 0px 100px 0px 100px;">Esses parâmetros, que implementam integridade de dados, aumentam a eficiência e reduzem custos, incluem:</p>
					</div>
				</div>
				<?php
				}else{
				?>
				<div class="row">
					<div class="col-md-12 col-sm-12">
						<p style="text-align:justify;margin: 0px 100px 0px 100px;">The core features of Core Fusion Middleware are built into the core of all modules, all in one code, with no need for integration.</p>
						<p style="text-align:justify;margin: 0px 100px 0px 100px;">These parameters, which implement data integrity, increase efficiency and reduce costs, include:</p>
					</div>
				</div>
				<?php
				}
				?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<div class="col-lg-4 col-md-4 col-sm-4 wow fadeInUp" data-wow-delay="0.6s">
					<i class="fas fa-code-branch fa-3x" style="color:#00c6d7;"></i>
						<h3><?=(($l == 'pt-br') ? 'Arquitetura inovadora' : 'Innovative architecture')?></h3>
						<hr>
						<!-- <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteu sunt in culpa qui officia.</p> -->
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4 wow fadeInUp" data-wow-delay="1s">
					<i class="fas fa-newspaper fa-3x" style="color:#00c6d7;"></i>
						<h3><?=(($l == 'pt-br') ? 'Aplicação responsiva' : 'Responsive aplication')?></h3>
						<hr>
						<!-- <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteu sunt in culpa qui officia.</p> -->
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4 wow fadeInUp" data-wow-delay="1s">
					<i class="fas fa-laptop-code fa-3x" style="color:#00c6d7;"></i>
						<h3><?=(($l == 'pt-br') ? 'Gestão LDAP/ACL' : 'LDAP/ACL menagement')?></h3>
						<hr>
						<!-- <p>You can edit and use this template for your websites. Please tell your friends about Tooplate. Thank you for visiting our website.</p> -->
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<div class="col-lg-4 col-md-4 col-sm-4 wow fadeInUp" data-wow-delay="0.9s">
					<i class="fas fa-project-diagram fa-3x" style="color:#00c6d7;"></i>
						<h3><?=(($l == 'pt-br') ? 'Fluxo de trabalho e processos' : 'Workflow and processes')?></h3>
						<hr>
						<!-- <p>Digital Team is free responsive Bootstrap v3.3.5 layout from <a rel="nofollow" href="http://www.tooplate.com" target="_parent">Tooplate</a>. Images are from <a rel="nofollow" href="http://pixabay.com" target="_parent">Pixabay</a> free photo website.</p> -->
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4 wow fadeInUp" data-wow-delay="1s">
					<i class="fas fa-spider fa-3x" style="color:#00c6d7;"></i>
						<h3><?=(($l == 'pt-br') ? 'Defesa contra hacker' : 'Hacker defence')?></h3>
						<hr>
						<!-- <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteu sunt in culpa qui officia.</p> -->
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4 wow fadeInUp" data-wow-delay="1s">
					<i class="fas fa-business-time fa-3x" style="color:#00c6d7;"></i>
						<h3><?=(($l == 'pt-br') ? 'Bibliotéca de APPS' : 'APPS library')?></h3>
						<hr>
						<!-- <p>You can easily change icons by looking at <a href="https://www.elegantthemes.com/blog/resources/how-to-use-and-embed-an-icon-font-on-your-website" target="_blank">ET Line Icons</a>. Excepteu sunt in culpa qui officia. Duis aute irure dolor in reprehenderit.</p> -->
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<div class="col-lg-4 col-md-4 col-sm-4 wow fadeInUp" data-wow-delay="1s">
					<i class="fas fa-cloud fa-3x" style="color:#00c6d7;"></i>
						<h3><?=(($l == 'pt-br') ? 'Nuvem nativa' : 'Native cloud')?></h3>
						<hr>
						<!-- <p>You can easily change icons by looking at <a href="https://www.elegantthemes.com/blog/resources/how-to-use-and-embed-an-icon-font-on-your-website" target="_blank">ET Line Icons</a>. Excepteu sunt in culpa qui officia. Duis aute irure dolor in reprehenderit.</p> -->
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4 wow fadeInUp" data-wow-delay="1s">
					<i class="fas fa-database fa-3x" style="color:#00c6d7;"></i>
						<h3><?=(($l == 'pt-br') ? 'Banco de dados analítico' : 'Analytical database')?></h3>
						<hr>
						<!-- <p>You can easily change icons by looking at <a href="https://www.elegantthemes.com/blog/resources/how-to-use-and-embed-an-icon-font-on-your-website" target="_blank">ET Line Icons</a>. Excepteu sunt in culpa qui officia. Duis aute irure dolor in reprehenderit.</p> -->
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4 wow fadeInUp" data-wow-delay="1s">
					<i class="fas fa-bezier-curve fa-3x" style="color:#00c6d7;"></i>
						<h3><?=(($l == 'pt-br') ? 'Web service integrado' : 'Integrated web service')?></h3>
						<hr>
						<!-- <p>You can easily change icons by looking at <a href="https://www.elegantthemes.com/blog/resources/how-to-use-and-embed-an-icon-font-on-your-website" target="_blank">ET Line Icons</a>. Excepteu sunt in culpa qui officia. Duis aute irure dolor in reprehenderit.</p> -->
				</div>
			</div>
		</div>
	</div>
</section>

<!-- about section -->
<section id="about">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12 text-center">
				<div class="section-title">
					<h1 class="heading bold"><?=(($l == 'pt-br') ? 'QUEM SOMOS' : 'WHO WE ARE')?></h1>
					<hr>
				</div>
			</div>
			<div class="col-md-6 col-sm-12">
				<img src="images/about-img.jpg" class="img-responsive" alt="about img">
			</div>
			<div class="col-md-6 col-sm-12">
				<h1 class="bold" style="color:#00c6d7;">Nucleo <b style="color:#ffff00;">O</b><b style="color:#00cc00;">S</b></h1>
				<h1 class="heading bold"><?=(($l == 'pt-br') ? 'Empresa de inovação tecnológica de código aberto' : 'Open Source Technology Innovation Company')?></h1>
				<!-- Nav tabs -->
				<ul class="nav nav-tabs" role="tablist">
					<li class="active"><a href="#design" aria-controls="design" role="tab" data-toggle="tab"><?=(($l == 'pt-br') ? 'EMPRESA' : 'COMPANY')?></a></li>
					<li><a href="#mobile" aria-controls="mobile" role="tab" data-toggle="tab"><?=(($l == 'pt-br') ? 'MISSÃO' : 'MISSION')?></a></li>
				</ul>
				<!-- tab panes -->
				<div class="tab-content">
					<?php
					if($l == 'pt-br'){
					?>
					<div role="tabpanel" class="tab-pane active" id="design">
						<p>A equipe NucleoOS consiste de um grupo de especialistas em gestão empresarial, escritório de Projetos,  segurança em tecnologia, mapeamento geo espacial e desenvolvedores de software 
 dentro dos padrões  internacionais de processos e workflow.</p>
					</div>
					<div role="tabpanel" class="tab-pane" id="mobile">
						<p>Promover serviços de tecnologia da informação de forma rápida e fluida, agregando valor a gestão de nossos clientes com eficiência e baixo custo.</p>
					</div>
					<?php
					}else{
					?>
					<div role="tabpanel" class="tab-pane active" id="design">
						<p>The NucleoOS team consists of a group of experts in business management, project office, technology security, geo-spatial mapping and software developers
 within the international standards of processes and workflow.</p>
					</div>
					<div role="tabpanel" class="tab-pane" id="mobile">
						<p>Promote information technology services quickly and smoothly, adding value to our customers' management with efficiency and low cost.</p>
					</div>
					<?php
					}
					?>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- vertica section -->
<section id="verticais">
	<div class="container">
		<div class="row">
			<br><br><br>
			<div class="col-md-12 col-sm-12 text-center">
				<div class="section-title">
					<h1 class="heading bold"><?=(($l == 'pt-br') ? 'VERTICAIS' : 'VERTICAL')?></h1>
					<hr>
				</div>
			</div>
			<div class="col-md-12 col-sm-12">
				<!-- <div class="section-title">
					<strong>03</strong>
					<h1 class="heading bold"><?=(($l == 'pt-br') ? 'VERTICAIS' : 'VERTICAL')?></h1>
					<hr>
				</div> -->
				<!-- ISO section -->
				<div class="iso-section">
					<ul class="filter-wrapper clearfix">
                   		 <li><a href="#" data-filter="*" class="selected opc-main-bg"><?=(($l == 'pt-br') ? 'Todos' : 'All')?></a></li>
                   		 <li><a href="#" class="opc-main-bg" data-filter=".hotel"><?=(($l == 'pt-br') ? 'Hotel' : 'Hotel')?></a></li>
                   		 <li><a href="#" class="opc-main-bg" data-filter=".governo"><?=(($l == 'pt-br') ? 'Governo' : 'Government')?></a></li>
                    	 <li><a href="#" class="opc-main-bg" data-filter=".saude"><?=(($l == 'pt-br') ? 'Saúde' : 'Health')?></a></li>
                    	 <li><a href="#" class="opc-main-bg" data-filter=".frotas"><?=(($l == 'pt-br') ? 'Frotas' : 'Fleet')?></a></li>
               		</ul>
               		<div class="iso-box-section wow fadeIn" data-wow-delay="0.9s">
               			<div class="iso-box-wrapper col4-iso-box">

               				 <div class="iso-box hotel col-lg-4 col-md-4 col-sm-6">
								<img src="images/portfolio-img1.jpg" alt="portfolio img" style="cursor:pointer;" onclick="showVertical(1,'<?=$l?>');">
               				 </div>

               				 <div class="iso-box governo col-lg-4 col-md-4 col-sm-6">
								<img src="images/portfolio-img2.jpg" alt="portfolio img" style="cursor:pointer;" onclick="showVertical(2,'<?=$l?>');">
               				 </div>

							 <div class="iso-box saude col-lg-4 col-md-4 col-sm-6">
								<img src="images/portfolio-img3.jpg" alt="portfolio img" style="cursor:pointer;" onclick="showVertical(3,'<?=$l?>');">
               				 </div>

							 <div class="iso-box frotas col-lg-4 col-md-4 col-sm-6">
								<img src="images/portfolio-img4.jpg" alt="portfolio img" style="cursor:pointer;" onclick="showVertical(4,'<?=$l?>');">
               				 </div>
	
               			</div>
               		</div>

				</div>
			</div>
		</div>
	</div>
</section>		

<!-- pricing section -->
<section id="pricing">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12 text-center">
				<div class="section-title">
					<h1 class="heading bold"><?=(($l == 'pt-br') ? 'PRODUTOS' : 'PRODUCTS')?></h1>
					<hr>
				</div>
			</div>
			<?php
			if($l == 'en'){
			?>
			<div class="col-md-4 col-sm-6">
				<div class="plan plan-one wow bounceIn" data-wow-delay="0.3s">
					<div class="plan_title">
						<i class="fa fa-database fa-2x"></i>
						<h3>Nucleo ADM</h3>
						<h3>Tools Database Management</h3>
					</div>
					<ul>
                    	<li>Interactive and Powerful</li>
						<li>Manage Multiple Databases</li>
						<li>Unified Workspace</li>
                        <li>Fast-performing Interface</li>
						<li>&nbsp;</li>
					</ul>
					<button class="btn btn-warning">Download</button>
				</div>
			</div>
			<div class="col-md-4 col-sm-6">
				<div class="plan plan-two wow bounceIn" data-wow-delay="0.3s">
					<div class="plan_title">
						<i class="fa fa-database fa-2x"></i>
						<h3>NucleoDB Enterprise</h3>
						<h3>Database Parallel Colunar</h3>
					</div>
					<ul>
						<li>Massive Parallel Database</li>
						<li>High Volume Transactional</li>
						<li>Business Intelligence Workloads</li>
                        <li>Compatible with PostgreSQL</li>
						<li>Live Support</li>
					</ul>
					<button class="btn btn-warning">Download</button>
				</div>
			</div>
			<div class="col-md-4 col-sm-6">
				<div class="plan plan-three wow bounceIn" data-wow-delay="0.3s">
					<div class="plan_title">
						<i class="fa fa-globe fa-2x"></i>
						<h3>NucleoOS Enterprise</h3>
						<h3>Nucleo Fusion Middleware</h3>
					</div>
					<ul>
						<li>Framework Development</li>
						<li>Webservice</li>
						<li>Database Parallel Colunar</li>
                        <li>Tools Database Management</li>
						<li>Live Supportt</li>
					</ul>
					<button class="btn btn-warning">Download</button>
				</div>
			</div>
			<?php
			}else{
			?>
			<div class="col-md-4 col-sm-6">
				<div class="plan plan-one wow bounceIn" data-wow-delay="0.3s">
					<div class="plan_title">
						<i class="fa fa-database fa-2x"></i>
						<h3>Nucleo ADM</h3>
						<h3>Ferramenta de gerenciamento de banco de dados</h3>
					</div>
					<ul>
                    	<li>Intereativa e poderosa</li>
						<li>Gerencie multiplos bancos de dados</li>
						<li>Espaço de trabalho unificado</li>
                        <li>Interface de alta performance</li>
						<li>&nbsp;</li>
					</ul>
					<button class="btn btn-warning">Baixar</button>
				</div>
			</div>
			<div class="col-md-4 col-sm-6">
				<div class="plan plan-two wow bounceIn" data-wow-delay="0.3s">
					<div class="plan_title">
						<i class="fa fa-database fa-2x"></i>
						<h3>NucleoDB Enterprise</h3>
						<h3>Base de dados paralela colunar</h3>
					</div>
					<ul>
						<li>Base de dados paralela massiva</li>
						<li>Alto volume de transações</li>
						<li>Cargas de trabalho em B.I.</li>
                        <li>Compatível com PostgreSQL</li>
						<li>Suporte</li>
					</ul>
					<button class="btn btn-warning">Baixar</button>
				</div>
			</div>
			<div class="col-md-4 col-sm-6">
				<div class="plan plan-three wow bounceIn" data-wow-delay="0.3s">
					<div class="plan_title">
						<i class="fa fa-globe fa-2x"></i>
						<h3>NucleoOS Enterprise</h3>
						<h3>Nucleo Fusion Middleware</h3>
					</div>
					<ul>
						<li>Framework de desenvolvimento</li>
						<li>Webservice</li>
						<li>Base de dados paralela colunar</li>
                        <li>Ferramenta de gestão de banco de dados</li>
						<li>Suporte</li>
					</ul>
					<button class="btn btn-warning">Baixar</button>
				</div>
			</div>
			<?php
			}
			?>
		</div>
	</div>		
</section>

<!-- contact section -->
<section id="contact">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12 text-center">
				<div class="section-title">
					<h1 class="heading bold"><?=(($l !== 'pt-br') ? 'PARTNERSHIP' : 'PARCERIA')?></h1>
					<hr>
				</div>
			</div>
			<div class="col-md-6 col-sm-12 contact-info">
				<h2 class="heading bold"><?=(($l == 'pt-br') ? 'INFORMAÇÃO DE PARCERIA' : 'PARTNERSHIP INFO')?></h2>
				<?php
				if($l == 'en'){
				?>
				<p>Contact us about anything related to our company or services, 
				We'll do our best to get back to you as soon as possible.</p>
				<?php
				}else if($l == 'pt-br'){
				?>
				<p>Nos contate sobre qualquer assunto relacionado a nossa empresa, produtos e serviços, 
				nós faremos o melhor para retornar o quanto antes.</p>
				<?php
				}
				?>
				<div class="col-md-6 col-sm-4">
					<h3><i class="icon-envelope medium-icon wow bounceIn" data-wow-delay="0.6s"></i> E-MAIL</h3>
					<p>suporte@nucleoos.tech</p>
				</div>
			</div>
			<div class="col-md-6 col-sm-12">
				<form action="#" method="post" class="wow fadeInUp" data-wow-delay="0.6s">
					<div class="col-md-6 col-sm-6">
						<input type="text" class="form-control" placeholder="<?=(($l == 'pt-br') ? 'Nome' : 'Name')?>" name="name">
					</div>
					<div class="col-md-6 col-sm-6">
						<input type="email" class="form-control" placeholder="E-mail" name="email">
					</div>
					<div class="col-md-6 col-sm-6">
						<input type="text" class="form-control" placeholder="<?=(($l == 'pt-br') ? 'Empresa' : 'Company')?>" name="company">
					</div>
					<div class="col-md-6 col-sm-6">
						<input type="email" class="form-control" placeholder="<?=(($l == 'pt-br') ? 'Contato' : 'Contact')?>" name="contact">
					</div>
					<div class="col-md-12 col-sm-12">
						<textarea class="form-control" placeholder="<?=(($l == 'pt-br') ? 'Mensagem' : 'Message')?>" rows="7" name="message"></textarea>
					</div>
					<div class="col-md-offset-4 col-md-8 col-sm-offset-4 col-sm-8">
						<input type="submit" class="form-control" value="<?=(($l == 'pt-br') ? 'ENVIAR MENSAGEM' : 'SEND MESSAGE')?>">
					</div>
				</form>
			</div>
		</div>
	</div>
</section>
<section id="final"></section>
<!-- footer section -->
<footer>
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<ul class="social-icon">
					<li><a href="https://www.facebook.com/profile.php?id=100009636378298" class="fa fa-facebook wow fadeIn" data-wow-delay="0.3s"></a></li>
					<li><a href="https://twitter.com/willybrand1" class="fa fa-twitter wow fadeIn" data-wow-delay="0.6s"></a></li>
					<li><a href="https://www.instagram.com/willybrand1/?hl=pt-br" class="fa fa-instagram wow fadeIn" data-wow-delay="0.9s"></a></li>
					<li><a href="https://plus.google.com/u/0/113753156843996641996" class="fa fa-google-plus wow fadeIn" data-wow-delay="0.9s"></a></li>
				</ul>
			</div>
		</div>
	</div>
</footer>
<div id='local-janela'></div>

<script src="./js/jquery.js"></script>
<script src="./js/bootstrap.min.js"></script>
<script src="./js/smoothscroll.js"></script>
<script src="./js/isotope.js"></script>
<script src="./js/imagesloaded.min.js"></script>
<script src="./js/nivo-lightbox.min.js"></script>
<script src="./js/jquery.backstretch.min.js"></script>
<script src="./js/wow.min.js"></script>
<script src="./js/custom.js"></script>
<?php
include_once DIR_FUNCOES.'funcoesJs.php';
?>

</body>
</html>
