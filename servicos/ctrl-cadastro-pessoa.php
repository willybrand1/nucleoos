<?php
include_once '../defines.php';
include_once DIR_FUNCOES.'funcoes.php';
include_once DIR_CLASSES.'pessoa.php';

$p = new Pessoa();

//set the values
$nome = $p->setNome($_REQUEST['nome']);
$cpf = $p->setCpf($_REQUEST['cpf']);
$dta_nascimento = $p->setDta_nascimento($_REQUEST['dta_nascimento']);
$rg = $p->setRg($_REQUEST['rg']);
$sexo = $p->setSexo($_REQUEST['sexo']);
$email = $p->setEmail($_REQUEST['email']);
$empresa = $p->setEmpresa($_REQUEST['empresa']);
$cnpj = $p->setCnpj($_REQUEST['cnpj']);
$telefone = $p->setTelefone($_REQUEST['telefone']);

//arrays and counts
$error = 0;
$arrErro = array();
$arrOk = array();
$arr = array();
$msg = "";

if($nome == "1"){
    $error += 1;
    array_push($arrErro,"1spNOME");
}else{
    array_push($arrOk,"spNOME");
}

if($cpf == "1"){
    $error += 1;
    array_push($arrErro,"1spCPF");
}else if(!validaCpf($p->getCpf($cpf))){
    $error += 1;
    array_push($arrErro,"2spCPF");
}else{
    array_push($arrOk,"spCPF");
} 

if($email == "1"){
    $error += 1;
    array_push($arrErro,"1spE-MAIL");
}else{
    array_push($arrOk,"spE-MAIL");
}

if($empresa == "1"){
    $error += 1;
    array_push($arrErro,"1spEMPRESA");
}else{
    array_push($arrOk,"spEMPRESA");
}

if($cnpj == "1"){
    $error += 1;
    array_push($arrErro,"1spCNPJ");
}else if(!validaCNPJ($p->getCnpj($cnpj))){
    $error += 1;
    array_push($arrErro,"2spCNPJ");
}else{
    array_push($arrOk,"spCNPJ");
}

if($telefone == "1"){
    $error += 1;
    array_push($arrErro,"1spTELEFONE");
}else{
    array_push($arrOk,"spTELEFONE");
}

if($error > 0){
    $arr['dados'] = ["cod"=>1];
    $arr['erros'] = $arrErro;
    $arr['acertos'] = $arrOk;
    echo json_encode($arr,JSON_UNESCAPED_UNICODE);
}else{
    //get the values
    $nome = $p->getNome($nome);
    $cpf = $p->getCpf($cpf);
    $dta_nascimento = $p->getDta_nascimento($dta_nascimento);
    $rg = $p->getRg($rg);
    $sexo = $p->getSexo($sexo);
    $email = $p->getEmail($email);
    $empresa = $p->getEmpresa($empresa);
    $cnpj = $p->getCnpj($cnpj);
    $telefone = $p->getTelefone($telefone);

    $sql = "INSERT INTO pessoa(nome,cpf,dta_nascimento,rg,sexo,email,empresa,cnpj,telefone) 
    VALUES('$nome','$cpf','$dta_nascimento','$rg','$sexo','$email','$empresa','$cnpj','$telefone');";
    $sql_t = "select * from pessoa;";
    
    $db = new Database(BANCO);
    
    $rs = $db->query($sql);
    $db->close();

    if(!$rs){
        $msg = "Erro retornado ao inserir dados no banco. Tente novamente.";
        $arr['dados'] = ["cod"=>2,"msg"=>$msg];
        echo json_encode($arr,JSON_UNESCAPED_UNICODE);
    }else{
        $msg = "Cadastro realizado com sucesso.";
        $arr['dados'] = ["cod"=>3,"msg"=>$msg];
        $arr['acertos'] = $arrOk;
        echo json_encode($arr,JSON_UNESCAPED_UNICODE);
    }
}
?>