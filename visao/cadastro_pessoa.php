<?php
include_once '../defines.php';
include_once DIR_VISAO.'header.php';
include_once DIR_FUNCOES.'funcoesJs.php';
?>
<script type="text/javascript">
    function abreCadastro(){
        var txt_t = '<span>Cadastro</span>';
        var txt = '';
        txt += '<div class="container">';
        txt += '    <div class="row">';
        txt += '        <div class="col-md-8 col-sm-12">';
        txt += '            <span id="spNOME"></span>';
        txt += '            <div class="input-group mb-3">';
        txt += '                <div class="input-group-prepend">';
        txt += '                    <span class="input-group-text" id="basic-addon3"><span class="c-req">*</span>Nome</span>';
        txt += '                </div>';
        txt += '                <input type="text" class="form-control" id="txtNome" name="txtNome">';
        txt += '            </div>';
        txt += '        </div>';
        txt += '    </div>';
        txt += '    <div class="row">';
        txt += '        <div class="col-md-5 col-sm-5">';
        txt += '            <span id="spCPF"></span>';
        txt += '            <div class="input-group mb-3">';
        txt += '                <div class="input-group-prepend">';
        txt += '                    <span class="input-group-text" id="basic-addon3"><span class="c-req">*</span>CPF</span>';
        txt += '                </div>';
        txt += '                <input type="text" class="form-control" id="txtCpf" name="txtCpf" maxlength="14">';
        txt += '            </div>';
        txt += '        </div>';
        txt += '    </div>';
        txt += '    <div class="row">';
        txt += '        <div class="col-md-7 col-sm-6">';
        txt += '            <span id="spDATA_DE_NASCIMENTO"></span>';
        txt += '            <div class="input-group mb-3">';
        txt += '                <div class="input-group-prepend">';
        txt += '                    <span class="input-group-text" id="basic-addon3">Data de nascimento</span>';
        txt += '                </div>';
        txt += '                <input type="date" class="form-control" id="txtDta_nascimento" name="txtDta_nascimento">';
        txt += '            </div>';
        txt += '        </div>';
        txt += '    </div>';
        txt += '    <div class="row">';
        txt += '        <div class="col-md-4 col-sm-5">';
        txt += '            <span id="spRG"></span>';
        txt += '            <div class="input-group mb-3">';
        txt += '                <div class="input-group-prepend">';
        txt += '                    <span class="input-group-text" id="basic-addon3">RG</span>';
        txt += '                </div>';
        txt += '                <input type="text" class="form-control" id="txtRG" name="txtRG">';
        txt += '            </div>';
        txt += '        </div>';
        txt += '    </div>';
        txt += '    <div class="row">';
        txt += '        <div class="col-md-4 col-sm-5">';
        txt += '            <span id="spSEXO"></span>';
        txt += '            <div class="input-group mb-3">';
        txt += '                <div class="input-group-prepend">';
        txt += '                    <span class="input-group-text" id="basic-addon3">Sexo</span>';
        txt += '                </div>';
        txt += '                <select class="form-control" id="txtRG" name="txtRG">';
        txt += '                    <option value="">&nbsp;</option>';
        txt += '                    <option value="Masculino">Masculino</option>';
        txt += '                    <option value="Feminino">Feminino</option>';
        txt += '                </select>';
        txt += '            </div>';
        txt += '        </div>';
        txt += '    </div>';
        txt += '    <div class="row">';
        txt += '        <div class="col-md-7 col-sm-6">';
        txt += '            <span id="spE-MAIL"></span>';
        txt += '            <div class="input-group mb-3">';
        txt += '                <div class="input-group-prepend">';
        txt += '                    <span class="input-group-text" id="basic-addon3"><span class="c-req">*</span>E-mail</span>';
        txt += '                </div>';
        txt += '                <input type="text" class="form-control" id="txtEmail" name="txtEmail">';
        txt += '            </div>';
        txt += '        </div>';
        txt += '    </div>';
        txt += '    <div class="row">';
        txt += '        <div class="col-md-7 col-sm-6">';
        txt += '            <span id="spEMPRESA"></span>';
        txt += '            <div class="input-group mb-3">';
        txt += '                <div class="input-group-prepend">';
        txt += '                    <span class="input-group-text" id="basic-addon3"><span class="c-req">*</span>Empresa</span>';
        txt += '                </div>';
        txt += '                <input type="text" class="form-control" id="txtEmpresa" name="txtEmpresa">';
        txt += '            </div>';
        txt += '        </div>';
        txt += '    </div>';
        txt += '    <div class="row">';
        txt += '        <div class="col-md-5 col-sm-5">';
        txt += '            <span id="spCNPJ"></span>';
        txt += '            <div class="input-group mb-3">';
        txt += '                <div class="input-group-prepend">';
        txt += '                    <span class="input-group-text" id="basic-addon3"><span class="c-req">*</span>CNPJ</span>';
        txt += '                </div>';
        txt += '                <input type="text" class="form-control" id="txtCnpj" name="txtCnpj" maxlength="18">';
        txt += '            </div>';
        txt += '        </div>';
        txt += '    </div>';
        txt += '    <div class="row">';
        txt += '        <div class="col-md-5 col-sm-5">';
        txt += '            <span id="spTELEFONE"></span>';
        txt += '            <div class="input-group mb-3">';
        txt += '                <div class="input-group-prepend">';
        txt += '                    <span class="input-group-text" id="basic-addon3"><span class="c-req">*</span>Telefone</span>';
        txt += '                </div>';
        txt += '                <input type="text" class="form-control" id="txtTelefone" name="txtTelefone">';
        txt += '            </div>';
        txt += '        </div>';
        txt += '    </div>';
        txt += '    <br>';
        txt += '    <div class="row">';
        txt += '        <div class="col-md-6 col-sm-6">';
        txt += '            <button onclick="salvaCadastroPessoa();" class="btn btn-primary">Salvar</button>';
        txt += '            <input type="button" class="btn btn-warning" value="Fechar" data-dismiss="modal">';
        txt += '        </div>';
        txt += '    </div>';
        txt += '</div>';

        abreJanelaModalTexto(txt_t, txt,'',1);
    }
</script>

<div id='local-janela'></div>
<div class="container">
    <br>
    <div class="row">
        <div class="col-md-4 col-sm-12">
            <button class="btn btn-primary" onclick="abreCadastro()">Download</button>
        </div>
    </div>
</div>
